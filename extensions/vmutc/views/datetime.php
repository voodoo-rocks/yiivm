<script type="text/javascript">
    (function($){
        'use strict';

        var referrerUrl = '<?=$referrer?>';

        var timeOffset = (new Date().getTimezoneOffset()) * (-1);

        $.ajax('<?=Yii::app()->createUrl(CHtml::normalizeUrl('timezone.php'))?>', {
            method: 'GET',
            data: {
                'currentDateTimeOffset': timeOffset
            },
            success: function () {
                if(referrerUrl){
                    window.location.href = referrerUrl;
                }
            }
        });
    })(window.jQuery);
</script>