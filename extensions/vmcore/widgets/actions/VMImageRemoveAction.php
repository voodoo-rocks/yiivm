<?php
/**
 * @class VMImageRemoveAction
 * Description of VMImageRemoveAction class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */
class VMImageRemoveAction extends CAction {
	public $model;
	public $attribute;

	public function run() {
		if(!$this->model) {
			throw new VMEntityException(Yii::t('vmcore.errors', '{property} is not set up properly', array('{property}' => 'model')));
		}

		if(!$this->attribute) {
			throw new VMEntityException(Yii::t('vmcore.errors', '{property} is not set up properly', array('{property}' => 'attribute')));
		}

		$attribute = $this->attribute;

		$saver = new VMEntitySaver($this->model);
		$saver->onAfterSave = function(CEvent $event) use ($attribute) {
			$upload = new VMUpload();
			$upload->quickRemove($event->sender->entity, $attribute);

			$event->sender->entity->{$attribute} = new CDbExpression('NULL');
			$event->sender->entity->save();
		};

		if(!$saver->save()) {
			CJSON::encode($saver->entity->errors);
		}
	}
}  {

} 