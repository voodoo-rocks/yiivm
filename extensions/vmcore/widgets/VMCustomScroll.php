<?php
/**
 * @class VMCustomScrollWidget
 * Description of VMCustomScrollWidget class
 *
 * @author Roman Solomaha <roman@voodoo-mobile.com>
 *
 * @var CClientScript $cs
 */

class VMCustomScroll extends CWidget {
	public $content;
	public $htmlOptions = array();
	public $options;

	public function init() {
		parent::init();

		$this->registerAssets();
	}

	public function run() {
		parent::run();

        if(array_key_exists('class', $this->htmlOptions)) {
            $this->htmlOptions['class'] = 'vm-scroller-wrapper ' . $this->htmlOptions['class'];
        } else {
            $this->htmlOptions['class'] = 'vm-scroller-wrapper';
        }

		$this->render('customScroll', array(
			'content' => $this->content,
            'htmlOptions' => $this->htmlOptions,
		));
	}

	protected function registerAssets() {
		$assetsPath = dirname(__DIR__) . DIRECTORY_SEPARATOR . 'assets';
		$assetsUrl  = Yii::app()->assetManager->publish($assetsPath, false, -1, YII_DEBUG);

		$cs = Yii::app()->clientScript;
		$cs->registerScriptFile($assetsUrl . '/js/vm-custom-scroll.js');
		$cs->registerScriptFile($assetsUrl . '/js/vm-custom-scroll-init.js');
		$cs->registerCssFile($assetsUrl . '/css/vm-custom-scroll.css');
	}
}