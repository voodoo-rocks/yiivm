<?php
/**
 * @var $content
 * @var $htmlOptions
 */
?>
<?= CHtml::openTag('div', $htmlOptions)?>
	<div class="vm-scroller">
		<div class="vm-scroller-container">
			<?php echo $content; ?>
		</div>
		<div class="vm-scroller-bar"></div>
	</div>
<?= CHtml::closeTag('div')?>