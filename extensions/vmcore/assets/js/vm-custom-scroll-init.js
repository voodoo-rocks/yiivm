/**
 * Created by roman-solomaha on 12.10.14.
 */

'use strict';

!(function($){
	$(window.document).ready(function(){
		baron($('.vm-scroller-wrapper'), {
			scroller: '.vm-scroller',
			container: '.vm-scroller-container',
			bar: '.vm-scroller-bar'
		});
	});
}(window.jQuery));