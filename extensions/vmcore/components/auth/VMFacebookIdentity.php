<?php

class VMFacebookIdentity extends CUserIdentity
{
	public $userClass;
	private $id = null;

	public function VMFacebookIdentity($facebook)
	{
		parent::__construct($facebook, null);
	}

	public function authenticate()
	{
		$model = call_user_func($this->user, 'model');
		if ($model) {
			$user = $model->findByAttributes(
				array(
					'facebook' => $this->username
				)
			);

			return $user !== null;
		}

		return false;
	}

	public function getId()
	{
		return $this->id;
	}
}