<?php
/**
 * @class VMTwitterService
 * Description of VMTwitterService class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */

class VMTwitterService extends TwitterOAuthService {
	protected function fetchAttributes() {
		$info = $this->makeSignedRequest('https://api.twitter.com/1.1/account/verify_credentials.json');

		$this->attributes['id'] = $info->id;
		$this->attributes['name'] = $info->name;
		$this->attributes['language'] = $info->lang;
	}
} 