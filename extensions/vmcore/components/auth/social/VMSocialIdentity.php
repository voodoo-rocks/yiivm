<?php
/**
 * @class VMSocialIdentity
 * Description of VMSocialIdentity class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */

class VMSocialIdentity extends EAuthUserIdentity
{
	public $userClass;
	public $identityField;

	public function VMSocialIdentity($userClass, $service, $identityField = 'id')
	{
		$this->userClass = $userClass;
		$this->identityField = $identityField;

		parent::__construct($service);
	}

	public function authenticate() {
		if (!$this->service->isAuthenticated) {
			return false;
		}

		$model = CActiveRecord::model($this->userClass);

		if(!$model) {
			return false;
		}

		$entity = $model->findByAttributes(
			array(
				'identity' => $this->service->id,
				'service'  => $this->service->serviceName,
			)
		);

		if ($entity) {
			$this->id = $entity->{$this->identityField};
		}

		return $entity !== null;
	}
}