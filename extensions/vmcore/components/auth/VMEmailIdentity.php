<?php

class VMEmailIdentity extends CUserIdentity
{
	public $afterAuth = null;
	public $user;
	protected $userClass;

	/**
	 * VMEmailIdentity constructor
	 *
	 * @param string $userClass
	 * @param string $email
	 * @param string $password
	 * @param array $params
	 */
	public function VMEmailIdentity($userClass, $email, $password)
	{
		$this->userClass = $userClass;

		parent::__construct($email, $password);
	}

	public function authenticate()
	{
		$model = CActiveRecord::model($this->userClass);
		if ($model) {
			$this->user = $model->findByAttributes(array(
					'email' => $this->username,
					'password' => $this->password
				)
			);

			if ($this->afterAuth) {
				call_user_func($this->afterAuth, new CEvent($this));
			}

			return $this->user !== null;
		}

		return false;
	}

	public function getId()
	{
		return $this->user->id;
	}
}
