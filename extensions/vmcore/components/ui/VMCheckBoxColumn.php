<?php
/**
 * Created by JetBrains PhpStorm.
 * User: Alex
 * Date: 29.07.13
 * Time: 4:05 AM
 * To change this template use File | Settings | File Templates.
 */

class VMCheckBoxColumn extends CGridColumn
{
	public $name;

	public function init()
	{
		parent::init();

		$this->headerHtmlOptions = array('class' => 'span1');
	}

	protected function renderHeaderCellContent()
	{
		echo CHtml::checkBox('All_' . $this->name, true);
	}

	protected function renderDataCellContent($row, $data)
	{
		echo CHtml::checkBox(get_class($data) . '[' . $data->primaryKey . ']', $data->{$this->name});
	}
}