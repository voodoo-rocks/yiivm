<?php
/**
 * @class VMEditableField
 * Description of VMEditableField class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */
class VMEditableField extends TbEditableField {
	public function buildJsOptions()
	{
		parent::buildJsOptions();

		if(!$this->source) {
			return;
		}

		if (is_array($this->source)) {
			if (isset($this->source[0]) && is_array($this->source[0])) {
				$source = $this->source;
			} else {
				$source = array();

				foreach ($this->source as $group => $items) {
					if(is_array($items)) {
						$children = array();

						foreach($items as $value => $title) {
							$children[] = array('value' => $value, 'text' => $title);
						}

						$source[] = array('text' => $group, 'children' => $children);
					} else {
						$source[] = array('value' => $group, 'text' => $items);
					}
				}
			}
		} else {
			$source = $this->source;
		}

		$this->options['source'] = $source;
	}
} 