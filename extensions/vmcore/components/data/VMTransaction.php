<?php

class VMTransaction extends CComponent
{
	private $transaction;

	public function __construct()
	{
		if(!Yii::app()->getDb()->currentTransaction) {
			$this->transaction = Yii::app()->getDb()->beginTransaction();
		}
	}

	public function execute($function)
	{
		try {
			$result = call_user_func($function, $this);
			$this->commit();
            return $result;

		} catch (Exception $exception) {
			$this->rollback();
			throw $exception;
		}
	}

	public function commit()
	{
		if ($this->transaction && $this->transaction->active) {
			$this->transaction->commit();
		}
	}

	public function rollback()
	{
		if ($this->transaction && $this->transaction->active) {
			$this->transaction->rollback();
		}
	}

	public function __destruct()
	{
		if ($this->transaction && $this->transaction->active) {
			$this->transaction->rollback();
		}
	}
}