<?php

/**
 * @class  VMFormValidator
 * Description of VMFormValidator class
 *
 * @property CFormModel $form
 */
class VMFormValidator extends CComponent
{
    private $formClass;
    private $form;
    private $loaded;

    public function __construct($formClass)
    {
        if (!$formClass) {
            throw new CException(Yii::t('vmcore.errors', '{property} is not set up properly', ['{property}' => 'formClass']));
        }

        $this->formClass = $formClass;
        $this->form      = new $formClass();

        $attributes = Yii::app()->request->getParam($this->formClass);
        if ($attributes) {
            $this->loaded           = true;
            $this->form->attributes = $attributes;
        }
    }

    public function validate($scenario = null, $ajaxValidate = false)
    {
        $this->form->setScenario($scenario);
        $result = false;

        if ($this->loaded) {
            if ($ajaxValidate) {
                $errors = CActiveForm::validate($this->form);

                echo $errors;

                if ($errors == CJavaScript::encode([])) {
                    $result = true;
                }
            } else {
                $result = $this->form->validate();
            }
        }

        return $result;
    }

    public function getForm()
    {
        return $this->form;
    }
}