<?php

/*
 * Converts geo degree -> km and km -> geo degree approximately
*/

class VMCoordinateConverter
{
	const METERS_PER_DEGREE_LATITUDE = 11132.22;
	const EARTH_PERIMETER_METERS = 40076000;
	const EARTH_RADIUS_METERS = 6378293;
	const TOTAL_DEGREES = 360;

	public static function metersToDegrees($meters)
	{
		return $meters / self::EARTH_PERIMETER_METERS * self::TOTAL_DEGREES;
	}

	public static function degreesToMeters($degrees)
	{
		return $degrees / self::TOTAL_DEGREES * self::EARTH_PERIMETER_METERS;
	}
}