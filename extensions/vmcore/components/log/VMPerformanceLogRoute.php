<?php

class VMPerformanceLogRoute extends CFileLogRoute
{
	protected function formatLogMessage($message, $level, $category, $time)
	{
		if (!ini_get('date.timezone')) {
			date_default_timezone_set('GMT');
		}

		$msMultiplier = 1000000;
		$micro = sprintf("%03d", ($time - floor($time)) * $msMultiplier);
		return sprintf('%s.%03d [%s] [%s] %s', date('Y-m-d H:i:s', $time), $micro, $level, $category, $message) . PHP_EOL;
	}
}