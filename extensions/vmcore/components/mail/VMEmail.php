<?php
Yii::import('yiivm.extensions.swiftMailer.SwiftMailer');

/**
 * @class  VMEmail
 * Description of VMEmail class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */
class VMEmail extends CComponent {
	const ADMIN_EMAIL_PARAM = 'adminEmail';

	const PLAIN_TEXT = 'text/plain';
	const HTML = 'text/html';

	protected $recipients;
	protected $subject;
	protected $message;
	protected $sender;

	protected $attachments = array();

	public function __construct ($recipients, $subject, $message = null, $sender = null) {
		$this->recipients = $recipients;
		$this->subject = $subject;
		$this->message = $message;
		$this->sender = ($sender) ? $sender : Yii::app()->params->itemAt(self::ADMIN_EMAIL_PARAM);
	}

	public function addAttachment ($attachments) {
		$this->attachments[] = $attachments;

		return $this;
	}

	public function send ($contentType = self::PLAIN_TEXT) {
		if(!$this->message) {
			throw new CException(Yii::t('vmcore.mail', 'Property "message" must be setting up'));
		}

		$mail = new SwiftMailer();
		$mail->init();

		/**
		 * @var Swift_Message $message
		 */
		$message = $mail->newMessage($this->subject)
		                ->setFrom($this->sender)
		                ->setReplyTo($this->sender)
		                ->setTo($this->recipients)
		                ->setBody($this->message, $contentType);

		if (count($this->attachments) > 0) {
			foreach ($this->attachments as $attachment) {
				if(is_array($attachment)) {
					foreach($attachment as $path => $name) {
						$message->attach(Swift_Attachment::fromPath($path)->setFilename($name));
					}
				} else {
					$message->attach(Swift_Attachment::fromPath($attachment));
				}
			}
		}

		$transport = $mail->sendmailTransport();
		$sender = $mail->mailer($transport);

		return $sender->send($message);
	}

	public static function quickSend ($recipients, $subject, $message = null, $sender = null) {
		if (!$sender) {
			$sender = Yii::app()->params->itemAt(self::ADMIN_EMAIL_PARAM);
		}

		if(!$sender) {
			throw new CException(Yii::t('vmcore.mail', 'Sender e-mail must be setting up'));
		}

		$headers = 'From: ' . $sender . PHP_EOL .
			'Reply-To: ' . $sender . PHP_EOL .
			'X-Mailer: PHP/' . phpversion() . PHP_EOL .
			'Content-Type: text/html; charset=utf-8' . PHP_EOL .
			'Content-Transfer-Encoding: 7bit';

		if (is_array($recipients)) {
			$result = true;

			foreach ($recipients as $recipient) {
				if(is_string($recipient)) {
					$result = $result && mail($recipient, $subject, $message, $headers);
				}
			}

			return $result;
		} else if (is_string($recipients)) {
			return mail($recipients, $subject, $message, $headers);
		}

		return false;
	}
} 