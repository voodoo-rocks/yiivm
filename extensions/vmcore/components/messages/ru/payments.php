<?php

return array(
    'Cardholder First Name' => 'Имя Пользователя Карты',
    'Cardholder Last Name' => 'Фамилия Пользователя Карты',
    'Card Number' => 'Номер Карты',
    'Card Expire Month' => 'Месяц Истечения Срока Карты',
    'Card Expire Year' => 'Год Истечения Срока Карты',
    'Card CVC code' => 'CVC Код Карты',
    'Card type "{type}" not supported' => 'Тип карты "{type}" не поддерживается',
    'Invalid payment provider' => 'Неверные данные оплаты',
);