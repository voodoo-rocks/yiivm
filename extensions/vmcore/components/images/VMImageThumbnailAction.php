<?php
Yii::import('yiivm.extensions.image.*');

class VMImageThumbnailAction extends CAction {
	const HTTP_STATUS_OK           = 200;
	const HTTP_STATUS_NOT_MODIFIED = 304;

	public $cacheInterval = 604800; //one week

	public $thumbnailWidth = 200;
	public $thumbnailHeight = 200;

	public $enableResize = true;
	public $enableCrop = true;

	protected $image;
	protected $size;
	protected $path;

	public function run ($path = null, $width = null, $height = null) {
		if ($width) {
			$this->thumbnailWidth  = $width;
			$this->thumbnailHeight = $width;
		}
		if ($height) {
			$this->thumbnailHeight = $height;
		}

		$this->path  = $path;
		$this->image = new Image($this->path);

		if($this->enableResize) {
			$this->makeAlignImageSize();
			$this->image->resize($this->size->width, $this->size->height, Image::NONE);
		}

		if($this->enableCrop) {
			$this->image->crop($this->thumbnailWidth, $this->thumbnailHeight);
		}

		$this->buildHeaders();
		$this->image->render();

		Yii::app()->end();
	}

	protected function buildHeaders () {
		$responseCode   = self::HTTP_STATUS_OK;
		$fileModifiedAt = filemtime($this->path);

		if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])) {
			$requestModifiedSince = new DateTime($_SERVER['HTTP_IF_MODIFIED_SINCE']);

			if ($requestModifiedSince->format('U') >= $fileModifiedAt) {
				$responseCode = self::HTTP_STATUS_NOT_MODIFIED;
			}
		}

		header('Cache-Control: public');
		header('Pragma: public');
		header('Expires: ' . gmdate('D, d M Y H:i:s', $fileModifiedAt + $this->cacheInterval) . ' GMT');
		header('Last-Modified: ' . gmdate('D, d M Y H:i:s', $fileModifiedAt) . ' GMT', true, $responseCode);
	}

	protected function makeAlignImageSize () {
		$size = (object)array(
			'realHeight'   => $this->image->height,
			'realWidth'    => $this->image->width,
			'customWidth'  => $this->thumbnailWidth,
			'customHeight' => $this->thumbnailHeight
		);

		$size->alignHeight      = $size->realHeight * ($size->customWidth / $size->realWidth);
		$size->alignWidth       = $size->realWidth * ($size->customHeight / $size->realHeight);
		$size->differenceHeight = $size->alignHeight - $size->customHeight;
		$size->differenceWidth  = $size->alignWidth - $size->customWidth;

		$isSmall = $size->realWidth < $size->customWidth && $size->realHeight < $size->customHeight;
		$isBig   = $size->realWidth > $size->customWidth && $size->realHeight > $size->customHeight;

		if ($isSmall) {
			$this->size = $this->sizeSmallToBig($size);
		} else if ($isBig) {
			$this->size = $this->sizeBigToSmall($size);
		} else if ($size->differenceHeight > $size->differenceWidth) {
			$this->size = $this->alignWidth($size);
		} else {
			$this->size = $this->alignHeight($size);
		}
	}

	protected function sizeSmallToBig ($size) {
		if ($size->differenceHeight > $size->differenceWidth) {
			return $this->alignHeight($size);
		}

		return $this->alignWidth($size);
	}

	protected function sizeBigToSmall ($size) {
		if ($size->differenceWidth < $size->differenceHeight) {
			return $this->alignHeight($size);
		}

		return $this->alignWidth($size);
	}

	protected function alignWidth ($size) {
		return (object)array(
			'height' => $size->customHeight,
			'width'  => $size->alignWidth
		);
	}

	protected function alignHeight ($size) {
		return (object)array(
			'height' => $size->alignHeight,
			'width'  => $size->customWidth
		);
	}
}