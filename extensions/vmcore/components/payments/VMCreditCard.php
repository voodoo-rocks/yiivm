<?php
/**
 * @class VMCreditCard
 * Description of VMCreditCard class
 *
 * @author Nikita Kolosov <nkolosov@voodoo-mobile.com>
 */
class VMCreditCard extends CModel {
	public $type;
	public $firstName;
	public $lastName;
	public $number;
	public $expireMonth;
	public $expireYear;
	public $cvc;

	public function rules()
	{
		return array(
			array('type, firstName, lastName, number, expireMonth, expireYear, cvc', 'safe'),
			array('type', 'in', 'range' => VMCreditCardType::getTypes()),
		);
	}

	public function attributeLabels()
	{
		return array(
			'firstName' => Yii::t('vmcore.payments', 'Cardholder First Name'),
			'lastName'  => Yii::t('vmcore.payments', 'Cardholder Last Name'),
			'number'    => Yii::t('vmcore.payments', 'Card Number'),
			'expireMonth' => Yii::t('vmcore.payments', 'Card Expire Month'),
			'expireYear'  => Yii::t('vmcore.payments', 'Card Expire Year'),
			'cvc'         => Yii::t('vmcore.payments', 'Card CVC code'),
		);
	}

	public function attributeNames()
	{
		$class = new ReflectionClass(get_class($this));
		$names = array();

		foreach($class->getProperties() as $property)
		{
			$name = $property->getName();
			if($property->isPublic() && !$property->isStatic())
				$names[] = $name;
		}

		return $names;
	}
}