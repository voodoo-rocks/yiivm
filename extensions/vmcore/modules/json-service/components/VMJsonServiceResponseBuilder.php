<?php

class VMJsonServiceResponseBuilder extends CComponent {
	/**
	 * @param null $data
	 * @param bool $return
	 *
	 * @return mixed
	 */
	public static function respond($data = null, $return = false) {
		return self::build($data, null, VMServiceResponseCode::NO_ERROR, $return);
	}

	/**
	 * @param      $mixed
	 * @param int  $code
	 * @param bool $return
	 *
	 * @return mixed
	 */
	public static function respondWithError($mixed, $code = VMServiceResponseCode::SERVICE_ERROR, $return = false) {
		$message = array();
		if (is_string($mixed)) {
			$message[] = array(
				'attribute' => null,
				'message'   => $mixed
			);
		} else if (is_array($mixed)) {
			foreach ($mixed as $attribute => $errors) {
				$message[] = array(
					'attribute' => is_string($attribute) ? $attribute : null,
					'message'   => is_array($errors) ? implode(', ', $errors) : $errors,
				);
			}
		}

		return self::build(array(), $message, $code, $return);
	}

	/**
	 * @param      $data
	 * @param      $exceptions
	 * @param      $code
	 * @param bool $return
	 *
	 * @return mixed
	 */
	public static function build($data, $exceptions, $code, $return = false) {
		$result = array(
			'result' => array(
				'succeeded'  => $exceptions == null,
				'exceptions' => $exceptions,
				'code'       => $code
			)
		);

		if ($data == null) {
			$data = array();
		}

		$response = array_merge($result, $data);

		if (function_exists('json_encode')) {
			$result = json_encode(array(
				'response' => $response
			));
		} else {
			$result = CJSON::encode(array(
				'response' => $response
			));
		}
		if ($return) {
			return $result;
		}

		header('Content-type: application/json');

		echo $result;
	}
}