<?php

class VMSecuredJsonServiceController extends VMJsonServiceController
{
	public $tokenClass = 'Token';

	public function init()
	{
		/** @noinspection PhpUndefinedFieldInspection */
		Yii::app()->user->logout(false);

		parent::init();

		/** @noinspection PhpUndefinedFieldInspection */
		if (!isset($this->request->token)) {
			return;
		}

		/** @noinspection PhpUndefinedFieldInspection */
		$identity = new VMServiceIdentity($this->request->token->hash, $this->tokenClass);

		if ($identity->authenticate()) {
			/** @noinspection PhpUndefinedFieldInspection */
			Yii::app()->user->login($identity, false);
		} else {
			$this->respondWithError(Yii::t('vmcore.json-service', 'Invalid token or authorization issue'));
		}
	}
}
