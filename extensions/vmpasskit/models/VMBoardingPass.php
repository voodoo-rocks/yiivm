<?php

class VMBoardingPass extends VMPass
{
	/** @var VMBoardingPassContent */
	public $boardingPass;

	public function __construct()
	{
		$this->barcode = new VMBarcode();
		$this->boardingPass = new VMBoardingPassContent();
	}
}