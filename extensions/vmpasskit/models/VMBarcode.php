<?php

class VMBarcode extends CComponent
{
	public $message;
	public $format = VMPassBarcodeFormat::PDF417;
	public $messageEncoding = 'iso-8859-1';
}